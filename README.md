Bayesian radio interferometric imaging with direction-dependent calibration
==========================================

This repository contains the code underlying the publication [Bayesian radio interferometric imaging with direction-dependent calibration](TODO: Add Link). Building on the [RESOLVE](https://gitlab.mpcdf.mpg.de/ift/resolve) framework, it demonstrates a direction-dependent calibration and imaging method on the example of VLA Cygnus A data. For a numerically efficient application of direction-dependent antenna gains in the measurement equation, the code relies on the image domain gridding algorithm. The differentiable implementation of the image domain gridding algorithm needed for this code can be found [here](https://gitlab.mpcdf.mpg.de/ift/public/idg)


Installation
------------

Install dependencies: [nifty8](https://gitlab.mpcdf.mpg.de/ift/nifty), [ducc](https://gitlab.mpcdf.mpg.de/mtr/ducc), [resolve](https://gitlab.mpcdf.mpg.de/ift/resolve), [idg](https://gitlab.mpcdf.mpg.de/ift/public/idg)

Calibration, imaging with direction-independent calibration, and imaging with direction-dependent calibration can be performed with the "rct_cal.py", "rct_die_im.py", and "rct_dde_im.py" scripts, respectively.

Please Note: These scripts are created for VLA Cygnus A data and will most likely need to be adapted for other data sets, especially when applied to other instruments.

Licensing terms
---------------

This package is licensed under the terms of the
[GPLv3](https://www.gnu.org/licenses/gpl.html) and is distributed
*without any warranty*.
