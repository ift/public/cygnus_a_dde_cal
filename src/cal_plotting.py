# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2023 Max-Planck-Society

import nifty8 as ift
import resolve as rve
import numpy as np
import matplotlib.pyplot as plt

from resolve.mpi import master

from .cal_model import calibrator_3C286_flux


def visualize_in_data_space(
    obs,
    vis_field,
    file_name_base,
    reconstruction=None,
    what=None,
    ant=None,
):
    assert obs.vis.domain == vis_field.domain

    if what == "real":
        func = lambda x: x.real
    elif what == "imag":
        func = lambda x: x.imag
    elif what == "abs":
        func = lambda x: np.abs(x)
    elif what == "angle":
        func = lambda x: np.angle(x)
    elif what is None:
        func = lambda x: x
    else:
        raise ValueError

    fig, axs = plt.subplots(18, 19, figsize=(30, 30))
    axs = list(axs.ravel())
    for ii, (ant1, ant2) in enumerate(obs.baselines()):
        if (ant is None) or (ant1 == ant) or (ant2 == ant):
            if len(axs) == 0:
                break
            axx = axs.pop(0)

            if what == "angle":
                mi, ma = -np.pi, np.pi
            else:
                mi, ma = np.min(func(vis_field.val)), np.max(func(vis_field.val))
            if what == "abs":
                mi = 0.0

            ind = np.logical_and(obs.ant1 == ant1, obs.ant2 == ant2)
            oo = obs[ind]
            assert oo.nfreq == 1
            for pol in range(oo.vis.shape[0]):
                ys = vis_field.val[pol, ind, 0]
                axx.scatter(oo.time, func(ys), s=1, c="red")

                if not reconstruction is None:
                    rct = reconstruction.val[pol, ind, 0]
                    axx.scatter(oo.time, func(rct), s=1, c="blue")
                axx.set_ylim([mi, ma])
                axx.set_title(str(ant1) + ", " + str(ant2))

    plt.tight_layout()
    if master:
        plt.savefig(f"{file_name_base}.png")
    fig.suptitle(what)
    plt.close()


def plot_cal(sl, output_dir, obs, cop0, cop1):
    for kk in obs.keys():
        cal_3C286_flux = calibrator_3C286_flux(obs[kk][0].freq[0] / 1e9)
        model_data = ift.makeOp(obs[kk][0].vis * 0 + cal_3C286_flux)
        rct = sl.average(model_data @ cop0[kk])
        visualize_in_data_space(
            obs[kk][0],
            obs[kk][0].vis,
            f"{output_dir}/angle_flux_{kk}",
            rct,
            "angle",
        )
        visualize_in_data_space(
            obs[kk][0],
            obs[kk][0].vis,
            f"{output_dir}/abs_flux_{kk}",
            rct,
            "abs",
        )
    for kk in obs.keys():
        rct = sl.average(cop1[kk])
        visualize_in_data_space(
            obs[kk][1],
            obs[kk][1].vis,
            f"{output_dir}/angle_phase_{kk}",
            rct,
            "angle",
        )
        visualize_in_data_space(
            obs[kk][1],
            obs[kk][1].vis,
            f"{output_dir}/abs_phase_{kk}",
            rct,
            "abs",
        )
