# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2023 Max-Planck-Society

import nifty8 as ift
import resolve as rve


def get_comm(i):
    return rve.mpi.comm


def map_optimize(lh, **kwargs):
    minimizer = ift.NewtonCG(ift.GradientNormController(name="kl", iteration_limit=40))
    it_cont = ift.AbsDeltaEnergyController(
        1e-1, iteration_limit=500, convergence_level=3
    )
    sl, mean = ift.optimize_kl(
        lh,
        1,
        0,
        minimizer,
        it_cont,
        None,
        return_final_position=True,
        comm=get_comm,
        **kwargs,
    )
    return sl, mean


def mgvi_optimize(lh, n_samp, n_iter, **kwargs):
    minimizer = ift.NewtonCG(ift.GradientNormController(name="kl", iteration_limit=20))
    it_cont = ift.AbsDeltaEnergyController(
        1e-1, iteration_limit=500, convergence_level=3
    )
    sl, mean = ift.optimize_kl(
        lh,
        n_iter,
        n_samp,
        minimizer,
        it_cont,
        None,
        return_final_position=True,
        comm=get_comm,
        **kwargs,
    )
    return sl, mean
