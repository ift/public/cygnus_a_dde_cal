# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2023 Max-Planck-Society

import numpy as np
import resolve as rve
import nifty8 as ift
import ducc0

from functools import reduce

from .rve_cfm_maker import cfm_from_cfg


def calibrator_3C286_flux(freq):
    x = np.log(freq) / np.log(10)
    return np.power(10, 1.2515 - 0.4605 * x - 0.1715 * x * x + 0.0336 * x * x * x)


def gen_weight_op(obs, mode, mean, key):
    w = (
        ift.InverseGammaOperator(obs.weight.domain, mode=mode, mean=mean).power(-1)
    ).ducktape(key)
    return ift.makeOp(obs.weight) @ w


def gen_jummp_op(jumps, domain, dt, mode, mean, key):
    time_domain = domain[1]
    inv_gamma_ops_fl = []
    n_ind = len(jumps) + 1
    n_t = time_domain.shape[0]
    t_max = n_t * dt
    ts = np.linspace(0, t_max, n_t)
    for ii in range(n_ind):
        ind = np.zeros(time_domain.shape[0])
        if ii == 0:
            beg = 0
        else:
            c = jumps[ii - 1]
            beg = np.searchsorted(ts, c)
        if ii == n_ind - 1:
            end = len(ind)
        else:
            c = jumps[ii]
            end = np.searchsorted(ts, c)
        ind[beg:end] = 1
        ind = np.broadcast_to(ind, domain.shape)
        ind = ift.makeField(domain, ind)
        key_s = f"{key}_sigma_{ii}"
        scl_dom = ift.DomainTuple.scalar_domain()
        flux_jump_sig = ift.InverseGammaOperator(
            scl_dom, mode=mode, mean=mean
        ).ducktape(key_s)
        unit = ift.full(domain[0], 1.0)
        key_xi = f"{key}_xi_{ii}"
        gauss = ift.makeOp(unit).ducktape(key_xi)
        expander1 = ift.ContractionOperator(domain[0], spaces=None).adjoint
        gauss = expander1(flux_jump_sig) * gauss
        expander = ift.ContractionOperator(domain, spaces=1).adjoint
        inv_gamma = expander @ gauss
        inv_gamma = ift.DiagonalOperator(ind) @ inv_gamma
        inv_gamma_ops_fl.append(inv_gamma)

    inv_gamma_fl = reduce(lambda i1, i2: i1 + i2, inv_gamma_ops_fl)
    return inv_gamma_fl


def built_cal_model(config, obs):
    nthreads = config["base"].getint("nthreads_rve")
    dt = 20  # s
    zero_padding_factor = 2

    phase = {}
    phase_flux = {}
    logamp = {}
    phase_jumps = {}
    phase_flux_jumps = {}
    logamp_jumps = {}

    weightop0 = {}
    weightop1 = {}
    weightop_imag = {}

    cop0 = {}
    cop1 = {}
    cop_imag = {}
    cop0_jumps = {}
    cop1_jumps = {}

    lh_flux_cal = {}
    lh_phase_cal = {}
    lh_flux_cal_jumps = {}
    lh_phase_cal_jumps = {}

    jumps_ph = {}
    jumps_ph["A"] = [
        200,
        1000,
        2000,
        3000,
        4000,
        5000,
        6000,
        7300,
        8200,
        9000,
        10000,
        11500,
        12500,
        13500,
        15000,
        16000,
        17000,
        18500,
        19500,
        21000,
        22000,
        23000,
        24500,
        25500,
    ]
    jumps_ph["B"] = [
        1500,
        2000,
        3000,
        4000,
        5000,
        6000,
        7500,
        8300,
        9000,
        10800,
        11500,
        12500,
        13500,
        14500,
        15500,
        16000,
        17000,
        18000,
        185000,
        19500,
        20500,
        21500,
        22000,
        23000,
        24000,
        24700,
        25500,
    ]
    jumps_ph["C"] = [
        2000,
        3000,
        4500,
        6000,
        7500,
        9000,
        10000,
        12000,
        13500,
        15000,
        16000,
        17000,
        18500,
        20000,
        21000,
        22000,
        235000,
        25000,
        26000,
        27000,
        28500,
        30000,
        31000,
        32000,
        33000,
        35000,
        36000,
    ]
    jumps_ph["D"] = [
        2000,
        3000,
        4500,
        6000,
        8000,
        9000,
        10000,
        11000,
        13000,
        14000,
        15000,
        16000,
        18000,
        19000,
        20000,
    ]

    for kk in obs.keys():
        uantennas = rve.unique_antennas(obs[kk][0], obs[kk][1], obs[kk][1])
        antenna_dct = {aa: ii for ii, aa in enumerate(uantennas)}
        tmin, tmax = rve.tmin_tmax(obs[kk][0], obs[kk][1], obs[kk][1])
        assert tmin == 0.0
        time_domain = ift.RGSpace(
            ducc0.fft.good_size(int(zero_padding_factor * (tmax - tmin) / dt)), dt
        )
        total_N = obs[kk][0].npol * len(uantennas)
        dofdex = np.arange(total_N)
        dd = {"time": time_domain}

        cfm_kwars = {"total_N": total_N, "dofdex": dofdex, "nthreads": nthreads}
        phase[kk] = cfm_from_cfg(
            config["die_phase"],
            dd,
            "die_phase",
            domain_prefix=f"die_phase_{kk}",
            **cfm_kwars,
        )
        phase_flux[kk] = cfm_from_cfg(
            config["die_phase_flux"],
            dd,
            "die_phase_flux",
            domain_prefix=f"die_phase_flux_{kk}",
            **cfm_kwars,
        )
        logamp[kk] = cfm_from_cfg(
            config["die_logamp"],
            dd,
            "die_logamp",
            domain_prefix=f"die_logamp_{kk}",
            **cfm_kwars,
        )

        jumps = gen_jummp_op(
            jumps_ph[kk], logamp[kk].target, dt, 1e-1, 1, f"logamp_jumps_{kk}"
        )
        logamp_jumps[kk] = jumps + logamp[kk]
        jumps = gen_jummp_op(
            jumps_ph[kk], phase[kk].target, dt, 1e-1, 1, f"phase_jumps_{kk}"
        )
        phase_jumps[kk] = jumps + phase[kk]

        pdom, _, fdom = obs[kk][0].vis.domain
        reshape = ift.DomainChangerAndReshaper(
            phase[kk].target,
            [pdom, ift.UnstructuredDomain(len(uantennas)), time_domain, fdom],
        )
        phase_flux[kk] = reshape @ phase_flux[kk]
        phase[kk] = reshape @ phase[kk]
        logamp[kk] = reshape @ logamp[kk]
        phase_jumps[kk] = reshape @ phase_jumps[kk]
        logamp_jumps[kk] = reshape @ logamp_jumps[kk]
        cop0[kk] = rve.calibration_distribution(
            obs[kk][0], phase_flux[kk], logamp[kk], antenna_dct, None
        )
        cop1[kk] = rve.calibration_distribution(
            obs[kk][1], phase[kk], logamp[kk], antenna_dct, None
        )
        cop0_jumps[kk] = rve.calibration_distribution(
            obs[kk][0], phase_flux[kk], logamp_jumps[kk], antenna_dct, None
        )
        cop1_jumps[kk] = rve.calibration_distribution(
            obs[kk][1], phase_jumps[kk], logamp_jumps[kk], antenna_dct, None
        )
        cop_imag[kk] = rve.calibration_distribution(
            obs[kk][2], phase[kk], logamp[kk], antenna_dct, None
        )

        expander = ift.ContractionOperator(cop1[kk].target, spaces=None).adjoint
        flux_op = ift.LognormalTransform(0.4, 0.3, "flux_scaling" + kk, 0)
        flux_e = expander @ flux_op
        flux_e = ift.Realizer(flux_e.target).adjoint @ flux_e
        cop1[kk] = flux_e * cop1[kk]
        cop1_jumps[kk] = flux_e * cop1_jumps[kk]

        weightop0[kk] = gen_weight_op(obs[kk][0], 1, 4, "w0" + kk)
        weightop1[kk] = gen_weight_op(obs[kk][1], 1, 4, "w1" + kk)
        weightop_imag[kk] = gen_weight_op(obs[kk][2], 0.6, 4, "w_im" + kk)

        cal_3C286_flux = calibrator_3C286_flux(obs[kk][0].freq / 1e9)
        lh_flux_cal[kk] = rve.CalibrationLikelihood(
            obs[kk][0],
            cop0[kk],
            obs[kk][0].vis * 0 + cal_3C286_flux,
            ift.log(weightop0[kk]),
        )
        lh_flux_cal[kk].name = "flux_cal_" + kk
        lh_flux_cal[kk].model_data = cop0[kk]

        lh_flux_cal_jumps[kk] = rve.CalibrationLikelihood(
            obs[kk][0],
            cop0_jumps[kk],
            obs[kk][0].vis * 0 + cal_3C286_flux,
            ift.log(weightop0[kk]),
        )
        lh_flux_cal_jumps[kk].name = "flux_cal_jumps_" + kk
        lh_flux_cal_jumps[kk].model_data = cop0_jumps[kk]

        lh_phase_cal[kk] = rve.CalibrationLikelihood(
            obs[kk][1], cop1[kk], obs[kk][1].vis * 0 + 1, ift.log(weightop1[kk])
        )
        lh_phase_cal[kk].name = "phase_cal_" + kk
        lh_phase_cal[kk].model_data = cop1[kk]

        lh_phase_cal_jumps[kk] = rve.CalibrationLikelihood(
            obs[kk][1], cop1_jumps[kk], obs[kk][1].vis * 0 + 1, ift.log(weightop1[kk])
        )
        lh_phase_cal_jumps[kk].name = "phase_cal_jumps_" + kk
        lh_phase_cal_jumps[kk].model_data = cop1_jumps[kk]

    model_dict = {}
    model_dict["phase_flux"] = phase_flux
    model_dict["phase"] = phase
    model_dict["logamp"] = logamp
    model_dict["cop0"] = cop0
    model_dict["cop1"] = cop1
    model_dict["cop0_jumps"] = cop0_jumps
    model_dict["cop1_jumps"] = cop1_jumps
    model_dict["cop_imag"] = cop_imag
    model_dict["weightop0"] = weightop0
    model_dict["weightop1"] = weightop1
    model_dict["weightop_imag"] = weightop_imag
    model_dict["lh_flux"] = lh_flux_cal
    model_dict["lh_phase"] = lh_phase_cal
    model_dict["lh_flux_jumps"] = lh_flux_cal_jumps
    model_dict["lh_phase_jumps"] = lh_phase_cal_jumps

    return model_dict
