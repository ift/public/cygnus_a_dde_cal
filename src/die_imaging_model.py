# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2023 Max-Planck-Society

import numpy as np
import resolve as rve
import nifty8 as ift
import ducc0

from .rve_cfm_maker import cfm_from_cfg
from .likelihood import CombinedImagingLikelihood


def built_imaging_model(config, obs, cop_imaging, weightop_imag):
    epsilon = config["base"].getfloat("epsilon")
    nthreads = config["base"].getint("nthreads_rve")

    sky_diffuse, pl_ops = rve.sky_model_diffuse(config["sky"])
    sky_points, pl_ops_points = rve.sky_model_points(config["sky"])
    sky = sky_diffuse + sky_points
    pl_ops = {**pl_ops, **pl_ops}

    ks = obs.keys()
    list_obs = [obs[kk][2] for kk in ks]
    list_cal = [cop_imaging[kk] for kk in ks]
    list_log_inv_cov = [ift.log(weightop_imag[kk]) for kk in ks]
    lh_imaging = CombinedImagingLikelihood(
        list_obs, sky, epsilon, True, list_log_inv_cov, list_cal, nthreads
    )

    model_dict = {}
    model_dict["sky"] = sky
    model_dict["pl_ops"] = pl_ops
    model_dict["lh_imaging"] = lh_imaging
    return model_dict
