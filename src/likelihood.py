# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2023 Max-Planck-Society

import nifty8 as ift
import numpy as np
import resolve as rve

from resolve.data.observation import Observation
from resolve.util import my_assert, my_assert_isinstance, my_asserteq
from functools import reduce
from operator import add
from .response import IDG2PolResponse


def combine_observations(observations_list):
    new_vis = np.concatenate([obs._vis for obs in observations_list], axis=1)
    new_uvw = np.concatenate([obs.uvw for obs in observations_list])
    new_ant1 = np.concatenate([obs.ant1 for obs in observations_list])
    new_ant2 = np.concatenate([obs.ant2 for obs in observations_list])
    new_time = np.concatenate([obs.time for obs in observations_list])
    new_weight = np.concatenate([obs._weight for obs in observations_list], axis=1)
    freq = observations_list[0].freq
    polarization = observations_list[0].polarization

    new_antenna_positions = rve.AntennaPositions(new_uvw, new_ant1, new_ant2, new_time)
    return rve.Observation(
        new_antenna_positions, new_vis, new_weight, polarization, freq
    )


def CombinedImagingLikelihood(
    observations_list,
    sky_operator,
    epsilon,
    do_wgridding,
    log_inverse_covariance_operator_list,
    calibration_operator_list,
    nthreads,
):
    joint_obs = combine_observations(observations_list)
    joint_cal = combine_ops(calibration_operator_list, 1, 1)
    joint_log_inv_cov = combine_ops(log_inverse_covariance_operator_list, 1, 1)
    return rve.ImagingLikelihood(
        observation=joint_obs,
        sky_operator=sky_operator,
        epsilon=epsilon,
        do_wgridding=do_wgridding,
        log_inverse_covariance_operator=joint_log_inv_cov,
        calibration_operator=joint_cal,
        nthreads=nthreads,
    )


def combine_ops(list_ops, axis, space):
    new_target = list(list_ops[0].target)
    new_len = sum(map(lambda op: op.target.shape[axis], list_ops))
    new_target[space] = ift.UnstructuredDomain(new_len)
    new_target = ift.DomainTuple.make(new_target)
    mf_op = [list_ops[i].ducktape_left(str(i)) for i in range(len(list_ops))]
    mf_op = reduce(lambda op1, op2: op1 + op2, mf_op)
    comb_op = CombOps(mf_op.target, new_target, axis=axis)
    return comb_op @ mf_op


class CombOps(ift.LinearOperator):
    def __init__(self, domain, target, axis):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(target)
        self._axis = axis
        self._capability = self.TIMES | self.ADJOINT_TIMES

        self._slc_start = [0]
        self._slc_end = []
        for i in range(len(domain.domains()) - 1):
            self._slc_start.append(self._slc_start[i] + domain.domains()[i].shape[axis])
        for i in range(len(domain.domains())):
            self._slc_end.append(self._slc_start[i] + domain.domains()[i].shape[axis])

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            xs = [fl.val for fl in x.values()]
            res = np.concatenate(xs, axis=self._axis)
            return ift.makeField(self._target, res)
        else:
            res = {}
            for i in range(len(self._slc_start)):
                res[self._domain.keys()[i]] = x.val.take(
                    range(self._slc_start[i], self._slc_end[i]), axis=self._axis
                )
            return ift.MultiField.from_raw(self._domain, res)


class Extraktor(ift.LinearOperator):
    def __init__(self, domain, target, slice):
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(target)
        self._slc = slice
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        if mode == self.TIMES:
            res = x.val[self._slc]
            return ift.Field(self.target, res.reshape(self.target.shape))
        else:
            res = np.zeros(self.domain.shape, dtype=x.dtype)
            res[self._slc] = x.val.reshape(res[self._slc].shape)
            return ift.Field(self.domain, res)


def _get_mask(observation):
    # Only needed for variable covariance gaussian energy
    my_assert_isinstance(observation, Observation)
    vis = observation.vis
    flags = observation.flags
    if not np.any(flags.val):
        return ift.ScalingOperator(vis.domain, 1.0), vis, observation.weight
    mask = observation.mask_operator
    return mask, mask(vis), mask(observation.weight)


def _Likelihood(operator, data, metric_at_pos, model_data):
    my_assert_isinstance(operator, model_data, ift.Operator)
    my_asserteq(operator.target, ift.DomainTuple.scalar_domain())
    my_asserteq(model_data.target, data.domain)
    operator.data = data
    operator.metric_at_pos = metric_at_pos
    operator.model_data = model_data
    return operator


def _build_gauss_lh_nres(op, mean, invcov):
    my_assert_isinstance(op, ift.Operator)
    my_assert_isinstance(mean, invcov, (ift.Field, ift.MultiField))
    my_asserteq(op.target, mean.domain, invcov.domain)

    lh = (
        ift.GaussianEnergy(
            mean, inverse_covariance=ift.makeOp(invcov, sampling_dtype=mean.dtype)
        )
        @ op
    )
    return _Likelihood(lh, mean, lambda x: ift.makeOp(invcov), op)


def _varcov(observation, Rs, inverse_covariance_operator):
    mosaicing = isinstance(observation, dict)
    s0, s1 = "residual", "inverse covariance"
    if mosaicing:
        lhs = []
        vis = {}
        masks = []
        for kk, oo in observation.items():
            mask = oo.mask_operator
            masks.append(mask.ducktape(kk).ducktape_left(kk))
            tgt = mask.target
            vis[kk] = mask(oo.vis)
            dtype = oo.vis.dtype
            a = ift.Adder(vis[kk], neg=True).ducktape_left(s0).ducktape("modeld" + kk)
            b = (
                ift.ScalingOperator(mask.target, 1.0)
                .ducktape_left(s1)
                .ducktape("icov" + kk)
            )
            e = ift.VariableCovarianceGaussianEnergy(tgt, s0, s1, dtype)
            lhs.append(e @ (a + b))
        masks = reduce(add, masks)

        a = ift.PrependKey(masks.target, "modeld") @ masks @ Rs
        b = ift.PrependKey(masks.target, "icov") @ masks @ inverse_covariance_operator
        lh = reduce(add, lhs) @ (a + b)

        vis = ift.MultiField.from_dict(vis)
        model_data = masks @ Rs
        icov_at = lambda x: ift.makeOp((masks @ inverse_covariance_operator).force(x))
    else:
        my_assert_isinstance(inverse_covariance_operator, ift.Operator)
        my_asserteq(
            Rs.target, observation.vis.domain, inverse_covariance_operator.target
        )
        mask, vis, _ = _get_mask(observation)
        residual = ift.Adder(vis, neg=True) @ mask @ Rs
        inverse_covariance_operator = mask @ inverse_covariance_operator
        dtype = observation.vis.dtype
        op = residual.ducktape_left(s0) + inverse_covariance_operator.ducktape_left(s1)
        lh = ift.VariableCovarianceGaussianEnergy(residual.target, s0, s1, dtype) @ op
        model_data = mask @ Rs
        icov_at = lambda x: ift.makeOp(inverse_covariance_operator.force(x))
    return _Likelihood(lh, vis, icov_at, model_data)


def PolCombIDGImagingLikelihood(
    observation,
    sky_operator,
    A_term_operator,
    antenna_dct,
    calibration_operator=None,
    calibration_field=None,
    inverse_covariance_operator=None,
    epsilon=1e-6,
    nthreads=1,
):
    from resolve.polarization_space import polarization_converter

    my_assert_isinstance(sky_operator, ift.Operator)
    my_assert_isinstance(A_term_operator, ift.Operator)
    sg_size = A_term_operator.target.shape[3:5]
    slc_A_ll = (
        slice(0, 1),
        slice(None),
        slice(None),
        slice(None),
        slice(None),
        slice(0, 1),
    )
    slc_A_rr = (
        slice(1, 2),
        slice(None),
        slice(None),
        slice(None),
        slice(None),
        slice(0, 1),
    )
    ex_a_l = Extraktor(A_term_operator.target, A_term_operator.target[1:4], slc_A_ll)
    ex_a_r = Extraktor(A_term_operator.target, A_term_operator.target[1:4], slc_A_rr)
    A_term = combine_ops([ex_a_l, ex_a_r], 0, 0)

    slc_ll = (slice(0, 1), slice(0, 1), slice(0, 1))
    sky = Extraktor(sky_operator.target, sky_operator.target[3], slc_ll) @ sky_operator
    sky = sky.ducktape_left("sky")
    mf = (A_term @ A_term_operator).ducktape_left("A") + sky
    model_data = (
        IDG2PolResponse(
            observation, mf.target, "A", "sky", antenna_dct, sg_size, epsilon, nthreads
        )
        @ mf
    )

    if calibration_operator is not None:
        reshape = ift.DomainChangerAndReshaper(
            model_data.target, calibration_operator.target
        )
    else:
        reshape = ift.DomainChangerAndReshaper(
            model_data.target, calibration_field.domain
        )
    model_data = reshape @ model_data
    if calibration_operator is not None and calibration_field is not None:
        raise ValueError(
            "Setting a calibration field and a calibration operator at the "
            "same time, does not work."
        )
    if calibration_operator is not None:
        model_data = calibration_operator * model_data
    if calibration_field is not None:
        model_data = ift.makeOp(calibration_field) @ model_data
    if inverse_covariance_operator is None:
        mask, vis, weight = _get_mask(observation)
        return _build_gauss_lh_nres(mask @ model_data, vis, weight)
    return _varcov(observation, model_data, inverse_covariance_operator)
