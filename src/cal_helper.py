# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2023 Max-Planck-Society

import nifty8 as ift

from .optimize import map_optimize


def single_obs_cal(obs_key, model_dict, save_inter=False, resume=False):
    if (not save_inter) and resume:
        raise ValueError("can only resume when intermediate results are stored")

    fd1 = f"interm/flux_cal1_{obs_key}" if save_inter else None
    fd2 = f"interm/flux_cal2_{obs_key}" if save_inter else None
    pd1 = f"interm/phase_cal1_{obs_key}" if save_inter else None
    pd2 = f"interm/phase_cal2_{obs_key}" if save_inter else None
    fpd = f"interm/cal_{obs_key}" if save_inter else None

    cst = model_dict["weightop0"][obs_key].domain.keys()
    cst += model_dict["weightop1"][obs_key].domain.keys()
    cst += model_dict["logamp"][obs_key].domain.keys()

    lh_flux = model_dict["lh_flux"][obs_key]
    _, mean_flux = map_optimize(
        lh_flux, constants=cst, output_directory=fd1, resume=resume
    )
    _, mean_flux = map_optimize(
        lh_flux, initial_position=mean_flux, output_directory=fd2, resume=resume
    )

    lh_phase = model_dict["lh_phase"][obs_key]
    _, mean_phase = map_optimize(
        lh_phase, constants=cst, output_directory=pd1, resume=resume
    )
    _, mean_phase = map_optimize(
        lh_phase, initial_position=mean_phase, output_directory=pd2, resume=resume
    )

    lh = lh_flux + lh_phase
    init_pos = 0.1 * ift.from_random(lh.domain)
    init_pos = ift.MultiField.union([init_pos, mean_flux])
    init_pos = ift.MultiField.union([init_pos, mean_phase])
    _, mean = map_optimize(
        lh, initial_position=init_pos, output_directory=fpd, resume=resume
    )

    return mean
