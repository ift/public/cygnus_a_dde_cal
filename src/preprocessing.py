# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2023 Max-Planck-Society

import numpy as np
import nifty8 as ift
import resolve as rve
import idg
import pickle

from os.path import join


def compute_emp_weighs(obs):
    from scipy.ndimage import gaussian_filter1d
    from scipy.signal import correlate, convolve

    weight = np.empty(obs.weight.domain.shape, dtype=np.float32)

    for ii, (ant1, ant2) in enumerate(obs.baselines()):
        ind = np.logical_and(obs.ant1 == ant1, obs.ant2 == ant2)
        oo = obs[ind]
        assert oo.nfreq == 1
        for pol in range(oo.vis.shape[0]):
            ys = obs.vis.val[pol, ind, 0]
            time = obs.time[ind]
            ys_filter = gaussian_filter1d(ys, sigma=2)
            delta = 0.3 * np.std(ys - ys_filter)
            w = 1 / (delta**2)
            weight[pol, ind, 0] = w

    return ift.makeField(obs.weight.domain, weight)


def compute_emp_weighs_avg(obs):
    from scipy.ndimage import gaussian_filter1d
    from scipy.signal import correlate, convolve

    weight = np.empty(obs.weight.domain.shape, dtype=np.float32)

    for ii, (ant1, ant2) in enumerate(obs.baselines()):
        ind = np.logical_and(obs.ant1 == ant1, obs.ant2 == ant2)
        oo = obs[ind]
        assert oo.nfreq == 1
        for pol in range(oo.vis.shape[0]):
            ys = obs.vis.val[pol, ind, 0]
            ts = obs.time[ind]
            ys_avg = np.empty_like(ys)
            dt = 20
            for ii in range(len(ys)):
                t = ts[ii]
                t_min = t - 4 * dt
                t_max = t + 4 * dt
                inds = (ts < t_max) & (ts > t_min)
                w = np.exp(-0.5 * ((ts[inds] - t) ** 2) / ((dt**2)))
                w = w / np.sum(w)
                ys_avg[ii] = np.average(ys[inds], weights=w)

            delta = 0.3 * np.std(ys - ys_avg)
            w = 1 / (delta**2)
            weight[pol, ind, 0] = w

    return ift.makeField(obs.weight.domain, weight)


def load_preprocessed_data(path):
    try:
        obs = pickle.load(open("obs_dict_preprocessed.pickle", "rb"))
        return obs
    except:
        pass

    ms_names = [
        "3C286-A",
        "3C286-C",
        "CYGNUS-A",
        "CYGNUS-C",
        "J2007-A",
        "J2007-C",
        "3C286-B",
        "3C286-D",
        "CYGNUS-B",
        "CYGNUS-D",
        "J2007-B",
        "J2023-D",
    ]

    obs = {}
    for ff in ms_names:
        if not ff[-1] in obs:
            obs_list = [None, None, None]
            obs[ff[-1]] = obs_list
        if ff[:-2] == '3C286':
            obs[ff[-1]][0] = rve.ms2observations(join(path, ff + ".ms"), "DATA", True, 0)[0]
        elif (ff[:-2] == 'J2007') or (ff[:-2] == 'J2023'):
            obs[ff[-1]][1] = rve.ms2observations(join(path, ff + ".ms"), "DATA", True, 0)[0]
        elif ff[:-2] == 'CYGNUS':
            obs[ff[-1]][2] = rve.ms2observations(join(path, ff + ".ms"), "DATA", True, 0)[0]
        else:
            print(ff[:-2])
            raise RuntimeError

    for kk in obs.keys():
        assert len(obs[kk]) == 3
        tmin, _ = rve.tmin_tmax(obs[kk][0], obs[kk][1], obs[kk][2])
        for ii in range(len(obs[kk])):
            obs[kk][ii] = obs[kk][ii].restrict_to_stokesi()
            obs[kk][ii] = obs[kk][ii].move_time(-tmin)
            if (ii == 2) and (kk == "A"):
                obs[kk][ii]._weight = compute_emp_weighs(obs[kk][ii]).val.astype(
                    np.float64
                )
            else:
                obs[kk][ii]._weight = compute_emp_weighs_avg(obs[kk][ii]).val.astype(
                    np.float64
                )
            obs[kk][ii]._vis = obs[kk][ii]._vis.astype(np.complex128)

    # convert to sorted observations for idg
    for kk in obs.keys():
        for ii in range(len(obs[kk])):
            uvw, ant1, ant2, time, t_bins, vis, freq, weight = idg.sort_observation(
                obs[kk][ii], -np.inf, np.inf, 10
            )
            sorted_ant_pos = rve.AntennaPositions(uvw, ant1, ant2, time)
            obs[kk][ii] = rve.Observation(
                sorted_ant_pos, vis, weight, obs[kk][ii].polarization, obs[kk][ii].freq
            )

    pickle.dump(obs, open("obs_dict_preprocessed.pickle", "wb"))
    return obs
