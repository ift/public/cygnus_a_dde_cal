# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2023 Max-Planck-Society

import numpy as np
import resolve as rve
import nifty8 as ift
import ducc0

from functools import reduce

from .rve_cfm_maker import cfm_from_cfg
from .likelihood import PolCombIDGImagingLikelihood


def remove_dep(op, spaces):
    dom = op.target
    u_op = ift.ScalingOperator(dom, 1.0)
    res = u_op
    for ii in spaces:
        d = dom[ii]
        iv = 1.0 / (d.dvol * np.prod(d.shape))
        io = iv * u_op.integrate(spaces=ii)
        expan = ift.ContractionOperator(dom, spaces=ii).adjoint
        res -= expan @ io
    return res @ op


def built_idg_model(config, obs, cop_imag, weight_imag, sky):
    nthreads = config["base"].getint("nthreads_rve")
    epsilon = config["base"].getfloat("epsilon")
    dt = 20

    sky_padding = 1.3125
    time_padding = 1.3

    sg_dom = ift.RGSpace((32, 32))
    sg_dom_padded = ift.RGSpace((int(sky_padding * 32), int(sky_padding * 32)))
    a_term_st = {}
    a_term_s = {}
    a_term = {}

    lh = []

    for kk in obs.keys():
        pdom, _, fdom = obs[kk][0].vis.domain
        uantennas = rve.unique_antennas(obs[kk][0], obs[kk][1], obs[kk][1])
        total_N = obs[kk][0].npol * len(uantennas)
        tmin, tmax = rve.tmin_tmax(obs[kk][0], obs[kk][1], obs[kk][1])
        assert tmin == 0.0
        nt_padded = ducc0.fft.good_size(int(time_padding * (tmax - tmin) / (20 * dt)))
        nt = int((tmax - tmin) / (20 * dt))
        time_domain_A = ift.RGSpace(nt, 20 * dt)
        time_domain_A_padded = ift.RGSpace(nt_padded, 20 * dt)
        dofdex = np.arange(total_N)
        dd = {"time": time_domain_A_padded, "space": sg_dom_padded}

        cfm_kwars = {"total_N": total_N, "dofdex": dofdex, "nthreads": nthreads}
        a_phase = cfm_from_cfg(
            config["a_terms"],
            dd,
            "ast_phase",
            domain_prefix=f"a_phase_{kk}",
            **cfm_kwars,
        )
        a_phase = ift.SliceOperator(a_phase.target, (total_N, nt, (32, 32))) @ a_phase
        a_phase = remove_dep(a_phase, [1, 2])
        a_phase = ift.Realizer(a_phase.target).adjoint @ a_phase

        a_logamp = cfm_from_cfg(
            config["a_terms"],
            dd,
            "ast_logamp",
            domain_prefix=f"a_logamp_{kk}",
            **cfm_kwars,
        )
        a_logamp = (
            ift.SliceOperator(a_logamp.target, (total_N, nt, (32, 32))) @ a_logamp
        )
        a_logamp = remove_dep(a_logamp, [1, 2])
        a_logamp = ift.Realizer(a_logamp.target).adjoint @ a_logamp

        dd = {"space": sg_dom_padded}
        a_phase_s = cfm_from_cfg(
            config["a_terms"],
            dd,
            "as_phase",
            domain_prefix=f"a_phase_s_{kk}",
            **cfm_kwars,
        )
        a_phase_s = ift.SliceOperator(a_phase_s.target, (total_N, (32, 32))) @ a_phase_s
        a_phase_s = ift.Realizer(a_phase_s.target).adjoint @ a_phase_s

        a_logamp_s = cfm_from_cfg(
            config["a_terms"],
            dd,
            "as_logamp",
            domain_prefix=f"a_logamp_s_{kk}",
            **cfm_kwars,
        )
        a_logamp_s = (
            ift.SliceOperator(a_logamp_s.target, (total_N, (32, 32))) @ a_logamp_s
        )
        a_logamp_s = ift.Realizer(a_logamp_s.target).adjoint @ a_logamp_s

        a_term_st[kk] = ift.exp(a_logamp + 1j * a_phase)
        a_term_s[kk] = ift.exp(a_logamp_s + 1j * a_phase_s)

        expan = ift.ContractionOperator(a_term_st[kk].target, spaces=1).adjoint
        a_term[kk] = ift.exp(
            a_logamp + expan(a_logamp_s) + 1j * (a_phase + expan(a_phase_s))
        )

        reshape_a = ift.DomainChangerAndReshaper(
            a_term[kk].target,
            [pdom, ift.UnstructuredDomain(len(uantennas)), time_domain_A, sg_dom, fdom],
        )
        reshape_a_ct = ift.DomainChangerAndReshaper(
            a_term_s[kk].target,
            [pdom, ift.UnstructuredDomain(len(uantennas)), sg_dom, fdom],
        )
        a_term[kk] = reshape_a @ a_term[kk]
        a_term_st[kk] = reshape_a @ a_term_st[kk]
        a_term_s[kk] = reshape_a_ct @ a_term_s[kk]

        uantennas = rve.unique_antennas(obs[kk][0], obs[kk][1], obs[kk][1])
        antenna_dct = {aa: ii for ii, aa in enumerate(uantennas)}
        lh[kk] = PolCombIDGImagingLikelihood(
            obs[kk][2],
            sky,
            a_term[kk],
            antenna_dct,
            cop_imag[kk],
            weight_imag,
            epsilon,
            nthreads,
        )
        lh[-1].name = "dde_" + kk

    lh = reduce(lambda l1, l2: l1 + l2, lh)

    model_dict = {}
    model_dict["a_term"] = a_term
    model_dict["a_term_st"] = a_term_st
    model_dict["a_term_s"] = a_term_s
    model_dict["lh"] = lh
    return model_dict
