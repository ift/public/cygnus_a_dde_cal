# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2023 Max-Planck-Society

import nifty8 as ift
import resolve as rve
import numpy as np
import idg


class IDG2PolResponse(ift.Operator):
    def __init__(
        self,
        observation,
        domain,
        A_term_key,
        sky_key,
        antenna_dct,
        sg_size=[32, 32],
        epsilon=1e-6,
        nthreads=1,
    ):
        assert isinstance(observation, rve.Observation)
        assert A_term_key == "A"
        assert sky_key == "sky"
        self._A_key = A_term_key
        self._s_key = sky_key

        sdom = domain[sky_key][0]

        nx_sky = sdom.shape[0]
        ny_sky = sdom.shape[1]
        dx = sdom.distances[0]
        dy = sdom.distances[1]
        fov_x = nx_sky * dx
        fov_y = ny_sky * dy
        ant1_pol1 = [antenna_dct[i] for i in observation.ant1]
        ant2_pol1 = [antenna_dct[i] for i in observation.ant2]
        max_ant = max(max(ant1_pol1), max(ant2_pol1))
        ant1_pol2 = [a1 + max_ant + 1 for a1 in ant1_pol1]
        ant2_pol2 = [a2 + max_ant + 1 for a2 in ant2_pol1]
        ant1 = ant1_pol1 + ant1_pol2
        ant2 = ant2_pol1 + ant2_pol2
        ntimes = domain[A_term_key].shape[1]
        nvis = observation.uvw.shape[0]
        uvw_shape = (2 * nvis, 3)
        uvw = np.empty(uvw_shape)
        uvw[0:nvis, :] = observation.uvw.copy()
        uvw[nvis : 2 * nvis, :] = observation.uvw.copy()
        uvw[:, 1] = -uvw[:, 1]
        time = np.empty(2 * nvis)
        time[0:nvis] = observation.time
        time[nvis : 2 * nvis] = observation.time

        self._gridder = idg.idg_response(
            domain,
            uvw,
            ant1,
            ant2,
            time,
            observation.freq,
            fov_x,
            fov_y,
            nx_sky,
            ny_sky,
            sg_size,
            epsilon,
            ntimes,
            nthreads,
        )
        self._domain = ift.makeDomain(domain)
        self._target = ift.makeDomain(ift.UnstructuredDomain(len(uvw)))

        self._vol = self._domain[sky_key][0].scalar_dvol

    def apply(self, x):
        self._check_input(x)
        return self._vol * self._gridder(x)
