# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2023 Max-Planck-Society

import nifty8 as ift
import resolve as rve
import configparser
import pickle

from functools import reduce

from src.preprocessing import load_preprocessed_data
from src.cal_model import built_cal_model
from src.die_imaging_model import built_imaging_model
from src.idg_model import built_idg_model
from src.optimize import mgvi_optimize


def get_comm(iglobal):
    return rve.mpi.comm


cfg = configparser.ConfigParser()
cfg.read("conf.cfg")
cfg_base = cfg["base"]
nthreads = cfg_base.getint("nthreads_rve")
nthreads_nifty = cfg_base.getint("nthreads_ift")
resume = cfg_base.getboolean("resume")
save_inter = cfg_base.getboolean("save_intermediate")
data_path = cfg_base["data_path"]


cfg_im = cfg["dde_im"]
seed = cfg_im.getint("random_seed")
output_dir = cfg_im["output_dir"]

ift.random.push_sseq_from_seed(seed)
ift.set_nthreads(nthreads_nifty)

obs = load_preprocessed_data(data_path)
model_dict_cal = built_cal_model(cfg, obs)
cop_imag = model_dict_cal["cop_imag"]
w_imag = model_dict_cal["weightop_imag"]
lh_phase_jumps = model_dict_cal["lh_phase_jumps"]
lh_flux_jumps = model_dict_cal["lh_flux_jumps"]

model_dict_imag = built_imaging_model(cfg, obs, cop_imag, w_imag)
pl_ops = model_dict_imag["pl_ops"]
sky = model_dict_imag["sky"]

model_dict_idg_imag = built_idg_model(cfg, obs, cop_imag, w_imag, sky)
lh_imag = model_dict_idg_imag["lh_imaging"]
pl_ops = model_dict_idg_imag["pl_ops"]

try:
    mean_imag = pickle.load(open("joint_im_and_cal_jumps/pickle/last.0.pickle", "rb"))
except:
    raise RuntimeError("Could not load die imaging. First run rct_die_im_jumps.py")


lh_list = []
for kk in obs.keys():
    lh_list.append(lh_flux_jumps[kk])
    lh_list.append(lh_phase_jumps[kk])
lh_list.append(lh_imag)

lh = reduce(lambda l1, l2: l1 + l2, lh_list)
init_pos = 0.1 * ift.from_random(lh.domain)
init_pos = ift.MultiField.union([init_pos, mean_imag])

sl_dde, mean_dde = ift.optimize_kl(
    lh,
    10,
    1,
    ift.NewtonCG(ift.GradientNormController(name="kl", iteration_limit=20)),
    # ift.NewtonCG(ift.GradientNormController(name="kl", iteration_limit=10), max_cg_iterations=700, energy_reduction_factor=1e-5),
    ift.AbsDeltaEnergyController(1e-1, iteration_limit=1000, convergence_level=3),
    None,
    return_final_position=True,
    initial_position=init_pos,
    comm=get_comm,
    output_directory=output_dir,
    save_strategy="all",
    export_operator_outputs=pl_ops,
    resume=True,
)
