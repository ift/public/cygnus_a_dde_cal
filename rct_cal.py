# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2023 Max-Planck-Society

import nifty8 as ift
import configparser

from functools import reduce

from src.preprocessing import load_preprocessed_data
from src.cal_model import built_cal_model
from src.cal_helper import single_obs_cal
from src.optimize import map_optimize
from src.cal_plotting import plot_cal

cfg = configparser.ConfigParser()
cfg.read("conf.cfg")
cfg_base = cfg["base"]
nthreads_nifty = cfg_base.getint("nthreads_ift")
resume = cfg_base.getboolean("resume")
save_inter = cfg_base.getboolean("save_intermediate")
data_path = cfg_base["data_path"]

print(data_path)

cfg_cal = cfg["cal"]
seed = cfg_cal.getint("random_seed")
output_dir = cfg_cal["output_dir"]

ift.random.push_sseq_from_seed(seed)
ift.set_nthreads(nthreads_nifty)

obs = load_preprocessed_data(data_path)
model_dict = built_cal_model(cfg, obs)


single_obs_mean = {}
for kk in obs.keys():
    single_obs_mean[kk] = single_obs_cal(kk, model_dict, save_inter, resume)

lh_list = []
lh_fl = model_dict["lh_flux"]
lh_ph = model_dict["lh_phase"]
for kk in obs.keys():
    lh_list.append(lh_fl[kk])
    lh_list.append(lh_ph[kk])

lh = reduce(lambda l1, l2: l1 + l2, lh_list)
init_pos = 0.1 * ift.from_random(lh.domain)
for kk in obs.keys():
    init_pos = ift.MultiField.union([init_pos, single_obs_mean[kk]])

sl, mean = map_optimize(
    lh, initial_position=init_pos, output_directory=output_dir, resume=resume
)


plot_cal(sl, output_dir, obs, model_dict["cop0"], model_dict["cop1"])
